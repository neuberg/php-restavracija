# Restavracija PHP



## Tehnologije

- **Front-end**: HTML 
- **Back-end**: PHP

## Funkcionalnosti

- Dodajanje izdelkov

- Brisanje izdelkov

- Filtriranje izdelkov po ceni, ključnih besedah in kategoriji

- Registracija/prijava/odjava

## Zaslonski posnetki

Glavna stran
![Glavna stran](slike/Screenshot_6.png)

----

Meni
![Meni](slike/meni.png)

----


Ponudba
![Ponudba](slike/ponudba.png)

----


Registracija
![Registracija](slike/registracija1.png)

----

Prijava
![Prijava](slike/prijava.png)

----

Dodajanje
![Dodajanje](slike/dodajnje.png)

----

Odjava
![Odjava](slike/odjava.png)



## Namestitev

Zahteve: [Nameščen XAMPP server](https://www.apachefriends.org)

1. Projektno nalogo premaknite v XAMPP/htdocs dodoteko

2. Ustvarite nov projekt na PhpMyAdmin z imenom "dsr" ter vanj uvozite "baza.sql"

3. V XAMPP kontrolnem oknu zaštartajte Apache ter MySQL

4. V brskalnik vtipkajte "localhost"



